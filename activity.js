// 1. Create an activity.js file on where to write and save the solution for the activity.


/*
	2. Find users with letter s in their first name or d in their last name.
		- Use the $or operator.
		- Show only the firstName and lastName fields and hide the _id field.
*/

db.users.find(
		{
			$or: [
				{firstName: {$regex: 's',}},
				{firstName: {$regex: 'S',}},
				{lastName: {$regex: 'd',}},
				{lastName: {$regex: 'D',}}

			]},
			
			{_id: 0, age: 0, contact: 0, courses: 0, department: 0}
	);

/*
	3. Find users who are from the HR department and their age is greater than or equal to 70.
		- Use the $and operator
*/

db.users.find(
		{
			$and: [
				{age: {$gte: 70}},
				{department: "HR"}

			]
		}
	);


/*
	4. Find users with the letter e in their first name and has an age of less than or equal to 30.
		- Use the $and, $regex and $lte operators
*/

db.users.find(
		{
			$and: [
				{firstName: {$regex: 'e',}},
				{age: {$lte: 30}},
			]
		}
	);